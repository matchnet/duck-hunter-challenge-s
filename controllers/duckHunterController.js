const express = require('express')
const duckHunterController = express.Router()
const duckHunterModel = require('../model/duckHunterChallengeSchema');
const { body } = require('express-validator');
const _ = require('lodash');
const TronWeb = require('tronweb');
const tronWeb = new TronWeb({
    fullNode: process.env.DEV_NODE_URL,
    solidityNode: process.env.DEV_NODE_URL,
    eventServer: process.env.DEV_NODE_URL,
    privateKey: process.env.CHALLENGES_WALLET_P_K
})
const utils = require('../utils/utils');

/** validate the params for bowling challenge */
duckHunterController.validateSaveChallenge = () => {
    return [
        body('title', 'Challenge title is required').exists(),
        body('noOfPlayer', 'noOfPlayer  is required').exists(),
        body('noOfTurns', 'noOfTurns  is required').exists(),
        body('challengeFee', 'challengeFee  is required').exists(),
        body('gameName', 'gameName  is required').exists(),
        // body('creator', 'creator  is required').exists(),
        body('winAmount', 'winAmount  is required').exists(),
    ]
}

/** create challenge object from req.body 
 * @param req
 */

duckHunterController.createChallenge = (req) => {
    return new Promise((resolve, reject) => {
        let params = req.body;
        let arr = [];
        let challenger = {
            player: params.playerId,
            isPlayed: false
        }
        arr.push(challenger);
        let obj = new duckHunterModel({
            title: params.title,
            noOfPlayer: params.noOfPlayer,
            noOfTurns: params.noOfTurns ? params.noOfTurns : 1,
            remainingPlayer: params.noOfPlayer - 1,
            challengeFee: params.challengeFee,
            gameName: params.gameName,
            creator: params.playerId,
            winAmount: params.winAmount,
            challengeType: params.challengeType ? params.challengeType : "High Score",
            challengers: arr,
            created_on: new Date(),
            updated_on: new Date()
        });
        duckHunterModel.saveDetails(obj).then((challenge) => {
            if (!challenge) {
                resolve(false);
            }
            resolve(challenge);
        }, error => {
            console.log("error in create challenge", error);
            reject(error);
        })
    })
}

duckHunterController.setScore = (req) => {
    return new Promise((resolve, reject) => {
        var scoreToUpdate = {
            playerId: _.get(req, 'body.playerId'),
            // walletId: _.get(req, 'body.walletId'),
            score: _.get(req, 'body.score', ''),
            // firstName: _.get(req, 'body.firstName', ''),
            // lastName: _.get(req, 'body.lastName', ''),
        };
        console.log("scoretoupdate", scoreToUpdate);
        duckHunterModel.updateChallenge(req.body.challengeId, scoreToUpdate).then((challenge) => {
            if (!challenge) {
                resolve(false);
            }
            resolve(challenge);
        }, error => {
            console.log("error in setscore", error);
            reject(error);
        })
    })
}

duckHunterController.processResultForChallenge = (challengeId, scores) => {
    return new Promise((resolve, reject) => {

        let scoreArr = [];
        for (let i = 0; i < scores.length; ++i) {
            let score = parseInt(_.get(scores[i], "score", 0));
            scoreArr.push(score);
        }
        let highScore = scoreArr[0];
        let maxIndex = 0;

        for (var i = 1; i < scoreArr.length; i++) {
            if (scoreArr[i] > highScore) {
                maxIndex = i;
                highScore = scoreArr[i];
            }
        }
        console.log("IDEally winner ==>", scores[maxIndex]);
        highScore = highScore.toString();
        let winner = _.get(scores[maxIndex], 'playerId._id', null);
        let winnerArr = [];
        winnerArr.push(winner);
        scores.forEach((element, index) => {
            if (element.score == scoreArr[maxIndex] && index != maxIndex && winner != (_.get(scores[index], 'playerId._id', null))) {
                winnerArr.push(_.get(scores[index], 'playerId._id', null));
            }
        });

        duckHunterModel.updateWinner(challengeId, winnerArr, highScore, 'close').then((response) => {
            if (!response) {
                //console.log("error in update winner", err);
                resolve(false);
            } else {
                resolve(response);
            }
        }, error => {
            console.log("error in update winner", error);
            reject(error);
        })
    })
}

duckHunterController.transferFundsToWinner = (walletId, winAmount) => {
    return new Promise(async(resolve, reject) => {
        //let winners = _.get(resultChallenge, 'winner', []);
        //console.log("Winner are", winners);
        //await asyncForEach(winners, async(winner) => {
        let to = walletId; //_.get(winner, 'walletid', ''); //winner wallet id
        let amount = winAmount; //(_.get(resultChallenge, 'winAmount', 0)) / winners.length;
        amount = (amount * 1000000); //win amount divided by number of winners
        console.log("to", to);
        console.log("amount", amount);
        try {
            await tronTransaction(to, amount);
            resolve(true);
        } catch (error) {
            console.log("error in tron transfer funds to winner", error);
            reject(error);
        }
        //})

    })
}

duckHunterController.transferFundsToParentGamingWallet = async(winAmount) => {
    try {
        return await tronTransaction(process.env.PARENT_GAMING_WALLET, winAmount * 1000000)
    } catch (error) {
        console.log("error in transferFundsToParentGamingWallet", error);
        return error;
    }
}

const tronTransaction = (to, amount) => {
    return new Promise((resolve, reject) => {
        tronWeb.trx.sendTransaction(to, amount).then((res) => {
            console.log("transfer Funds res", res);
            resolve(res)
        }, err => {
            console.log("transfer funds error", err);
            reject(err);
        })
    })
}

const asyncForEach = async(array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index]);
    }
}

duckHunterController.creditFundsToGamingWallet = (walletId, winAmount, challengeId, token) => {
    return new Promise(async(resolve, reject) => {
        //let winners = _.get(resultChallenge, 'winner', []);
        //console.log("Winner are", winners);
        //await asyncForEach(winners, async(winner) => {
        let to = walletId; //_.get(winner, 'walletid', ''); //winner wallet id
        let amount = winAmount; //(_.get(resultChallenge, 'winAmount', 0)) / winners.length;
        amount = (amount * 1000000); //win amount divided by number of winners
        console.log("to", to);
        console.log("amount", amount);

        try {
            await utils.creditToGamingWallet(to, amount, `challenge : ${challengeId}`, token);
            resolve(true);
        } catch (error) {
            console.log("error in creditFundsToGamingWallet", error);
            reject(error);
        }
        //})

    })
}

//const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

// Sample how to use asyncforeach
// const start = async () => {
//     await asyncForEach([1, 2, 3], async (num) => {
//       await waitFor(50);
//       console.log(num);
//     });
//     console.log('Done');
//   }


module.exports = duckHunterController;