const Express = require("express");
const BodyParser = require("body-parser");
const Dotenv = require('dotenv').config();
const DB = require("./model/db");
const duckHunterRoute = require("./routes/duckHunterRoute");
const jwtMiddleware = require("./middlewares/jwt");
let app = Express();


app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('x-Trigger', 'CORS');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, clientdomain, x-access-token, encrypted');
    next();
};

const cors = require('cors');
const whitelist = ['http://localhost:4200', 'http://loyaltywink.vidit.webfactional.com']
const corsOptions = {
    origin: function(origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}
app.use(allowCrossDomain);
app.use(cors());
app.use(jwtMiddleware);
app.use("/duckHunter", duckHunterRoute);

/** comment this code for AWS Lambda , Uncomment for local dev*/
// const server = app.listen(3000, () => {
//     console.log("server started");
// });


module.exports = app;

// ANY - https://fg0ueprk42.execute-api.us-east-2.amazonaws.com/dev/duckHunter