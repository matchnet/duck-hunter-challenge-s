const express = require("express");
const duckHunterChallenges = express.Router();
const duckHunterModel = require('../model/duckHunterChallengeSchema');
const userModel = require('../model/user');
const duckHunterCtrl = require('../controllers/duckHunterController');
const user = require('../model/user');
const MOMENT = require('moment')
const _ = require('lodash');
//const validator = require('validator');
const schedule = require('node-schedule');
const TronWeb = require('tronweb');
const utils = require('../utils/utils');
const { validationResult } = require("express-validator");
const response = require('../utils/response');
const mongoose = require("mongoose");
//let asyncTask = null;
let challengeAsync = null;
let timer = 0;

// account for croupier
// const tronWeb = new TronWeb({
//     fullNode: process.env.DEV_NODE_URL,
//     solidityNode: process.env.DEV_NODE_URL,
//     eventServer: process.env.DEV_NODE_URL,
//     privateKey: process.env.CROUPIERS_P_K
// })

const emitSocket = (eventName, response, msg) => {
    return new Promise(async(resolve, reject) => {
        let success = await utils.sendSocket(eventName, response, msg);
        //console.log('emitSocket function called');
        if (success) {
            resolve(success);
        } else {
            reject(false);
        }
    })

}

const sendSocketForChallengeWalletDetails = async() => {
    try {
        let tronWeb = await new TronWeb({
            fullNode: process.env.STAGING_SHASTANET_URL,
            solidityNode: process.env.STAGING_SHASTANET_URL,
            eventServer: process.env.STAGING_SHASTANET_URL,
            privateKey: process.env.CHALLENGES_WALLET_P_K
        });
        let challengeWalletBalance = await tronWeb.trx.getBalance(process.env.CHALLENGES_WALLET);
        challengeWalletBalance = challengeWalletBalance / 1000000;
        let obj = {
            address: process.env.CHALLENGES_WALLET,
            currentBalance: challengeWalletBalance
        }
        emitSocket("challenge_wallet", obj, "Challenge Wallet Status");
    } catch (error) {
        console.log("error in sendSocketForChallengeWalletDetails", error);
    }
}

duckHunterChallenges.get('/skillchallenge', (req, res, next) => {
    res.send("skillGameChallenge working OK");
});

/**
 * This function comment is parsed by doctrine
 * @route POST /saveskillchallenge
 * 
 */
duckHunterChallenges.post('/saveskillchallenge', duckHunterCtrl.validateSaveChallenge(), async(req, res) => {
    try {
        sendSocketForChallengeWalletDetails();

        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() });
            return;
        }

        const saveChallenge = await duckHunterCtrl.createChallenge(req);
        if (!saveChallenge) {
            res.send(response.failure("Some error in creating challenge"));
            return;
        }
        await emitSocket('bowling_challenge_published', saveChallenge, (_.get(req, 'body.playerName') ? _.get(req, 'body.playerName') : _.get(saveChallenge, '_id')) + " has published a challenge " + _.get(req, 'body.title'));
        res.send(response.success(saveChallenge));
    } catch (error) {
        console.log("error in /saveskillchallenge", error);
        res.send(response.failure(error));
    }

});

/**
 * @param playerId
 * @param challengeId
 * @param playerName
 */
duckHunterChallenges.post('/preBook', async(req, res) => {
    try {
        emitSocket('skill_check_availability', req.body, 'Checking availability for joining challenge');
        if (!req.body.playerId || !req.body.challengeId) {
            res.send(response.failure("Player Id (playerId) and Challenge Id (challengeId) are required fields"));
            return;
        }

        if (challengeAsync && challengeAsync == req.body.challengeId) {
            //await sleep();
            //asyncTask = null;
            timer = timer + Math.floor(Math.random() * (1000 - 500) + 500);
        } else {
            timer = 0;
        }
        //asyncTask = true;
        challengeAsync = req.body.challengeId;
        //let previousTimer = timer;
        //do {
        console.log("timer", timer);

        //} while (timer - previousTimer > 500)


        setTimeout(async() => {
            try {
                asyncTask = await preBook(req);
                console.log("AsyncTask timer", timer);
                timer = 0;
                res.send(response.success("success"));
            } catch (error) {
                console.log("error in /preBook", error);
                console.log("AsyncTask timer", timer);
                res.send(response.failure(error));
            }
        }, timer);





    } catch (error) {
        console.log("error in /preBook", error);
        res.send(response.failure(error));
    }
});

const sleep = () => {
    return new Promise((resolve, reject) => {
        let timer = Math.floor(Math.random() * (1000 - 500) + 500);
        console.log("timer", timer);
        setTimeout(() => {
            console.log("sleep finished");
            resolve();
        }, timer);
    })
}

const preBook = (req) => {
    return new Promise(async(resolve, reject) => {
        let challenge = await duckHunterModel.getChallengeById(req.body.challengeId);
        console.log("challenge.remainingplayer", challenge.remainingPlayer);
        if (!challenge) {
            //res.send(response.failure("Some error in finding the challenge with this challenge Id"));
            //return;
            return reject("Some error in finding the challenge with this challenge Id");
        }
        if (challenge.remainingPlayer < 0 && challenge.status == 'open') {
            let updatedChallenge = await duckHunterModel.updateChallengeForPreAndCancelBook(req.body.challengeId, req.body.playerId, false);
            console.log("updatedChallenge", updatedChallenge);
            if (!updatedChallenge) {
                return reject("Some error in challenge update");
            }
            if (updatedChallenge.remainingPlayer <= 0) {
                //res.send(response.notAllowed("Someone just took the challenge. All slots are booked."));
                //return;
                return reject("Someone is in process of booking the slot for this challenge.");
            }
        }
        if (challenge.remainingPlayer <= 0) {
            return reject("Someone is in process of booking the slot for this challenge.");
        }
        console.log("came here");
        let updatedChallenge = await duckHunterModel.updateChallengeForPreAndCancelBook(req.body.challengeId, req.body.playerId, true);
        if (!updatedChallenge) {
            //res.send(response.failure("Some error in challenge update"));
            //return;
            return reject("Some error in challenge update")
        }
        return resolve(true);

    })
}


/**
 * @param playerId
 * @param challengeId
 * @param playerName
 */
duckHunterChallenges.post('/cancelPreBook', async(req, res) => {
    try {
        //emitSocket('skill_check_availability', req.body, 'Checking availability for joining challenge');
        if (!req.body.playerId || !req.body.challengeId) {
            res.send(response.failure("Player Id (playerId) and Challenge Id (challengeId) are required fields"));
            return;
        }
        let challenge = await duckHunterModel.getChallengeById(req.body.challengeId);
        if (!challenge) {
            res.send(response.failure("Some error in finding the challenge with this challenge Id"));
            return;
        }
        let updatedChallenge = await duckHunterModel.updateChallengeForPreAndCancelBook(req.body.challengeId, req.body.playerId, false);
        if (!updatedChallenge) {
            res.send(response.failure("Some error in challenge update"));
            return;
        }
        console.log("cancel prebook challenge", updatedChallenge);
        res.send(response.success("success"));

    } catch (error) {
        console.log("error in /cancelPreBook", error);
        res.send(response.failure(error));
    }
})

/**
 * @param playerId
 * @param challengeId
 * @param playerName
 */
duckHunterChallenges.post('/bookChallenge', async(req, res) => {
    try {

        if (!req.body.playerId || !req.body.challengeId) {
            res.send(response.failure("Player Id (playerId) and Challenge Id (challengeId) are required fields"));
            return;
        }

        let challenge = await duckHunterModel.getChallengeById(req.body.challengeId);
        if (!challenge) {
            res.send(response.failure("Some error in finding the challenge with this challenge Id"));
            return;
        }

        // if (challenge.remainingPlayer <= 0) {
        //     res.send(response.notAllowed("All slots are booked."));
        //     return;
        // }
        // check if this is the last player to join
        let updatedChallenge = null;
        if (challenge.remainingPlayer == 0) {
            updatedChallenge = await duckHunterModel.updateChallenger(req.body.challengeId, req.body.playerId, 'start');
        } else {
            updatedChallenge = await duckHunterModel.updateChallenger(req.body.challengeId, req.body.playerId, null);
        }

        if (!updatedChallenge) {
            res.send(response.failure("Some error in challenge update"));
            return;
        }
        await emitSocket('bowling_challenge_accepted', updatedChallenge, (_.get(req, 'body.playerName') ? _.get(req, 'body.playerName') : _.get(req, 'body.playerId')) + " has accepted the challenge " + _.get(updatedChallenge, 'title'));
        res.send(response.success(updatedChallenge));
    } catch (error) {
        console.log("error in /bookChallenge", error);
        res.send(response.failure(error));
    }
});


/**
 * // post the scores for challengers
 * @param playerId
 * 
 * @param score // send as string
 * @param playerName
 * 
 * @param challengeId 
 */

const asyncForEach = async(array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], );
    }
}
duckHunterChallenges.post('/score', async(req, res) => {
    let token = req.headers['x-access-token'] || req.headers['authorization'];
    console.log("token", token);
    try {
        sendSocketForChallengeWalletDetails();

        if (!req.body.playerId || !req.body.challengeId || !req.body.score) {
            res.send(response.failure("Player Id (playerId) , Challenge Id (challengeId) and score are required fields"));
            return;
        }

        let challenge = await duckHunterModel.getChallengeById(req.body.challengeId);
        if (!challenge) {
            res.send(response.failure("Some error in finding the challenge with this challenge Id"));
            return;
        }

        let isChallenger = false;
        await asyncForEach(challenge.challengers, async(challenger) => {
            // console.log(challenger.player);
            // console.log(req.body.playerId);
            if (challenger.player._id == req.body.playerId) {
                isChallenger = true;
            }
        })
        if (!isChallenger) {
            res.send(response.notAllowed("You are not authorize to play in this challenge"));
            return;
        }
        //check if user has already posted the score
        let isScorePosted = true;
        await asyncForEach(challenge.challengers, async(challenger) => {

            if (challenger.player._id == req.body.playerId && challenger.isPlayed == false) {
                //console.log("herer");
                isScorePosted = false;
            }
        });

        if (isScorePosted) {
            res.send(response.notAllowed("You have already played your turn for this challenge"));
            return;
        }
        console.log("player score is", req.body.score)
        let updateChallenge = await duckHunterCtrl.setScore(req);
        if (!updateChallenge) {
            res.send(response.failure("some error in updating challenge with score"));
            return;
        }

        console.log(JSON.stringify(updateChallenge));
        await emitSocket('bowling_challenge_updated', updateChallenge, `${req.body.playerName ? req.body.playerName : req.body.playerId} has scored ${req.body.score} for ${updateChallenge.gameName} challenge  ${updateChallenge.title}`);
        //check if challenge should be executed to announce winner//
        let didAllChallengerPlayed = true;
        await asyncForEach(challenge.challengers, async(challenger) => {
            if (!challenger.isPlayed) {
                didAllChallengerPlayed = false;
            }
        })


        if (updateChallenge.scores.length == updateChallenge.noOfPlayer ||
            didAllChallengerPlayed && updateChallenge.remainingPlayer == 0) {
            // execute the challenge and announce the winner //
            let executeChallenge = await duckHunterCtrl.processResultForChallenge(updateChallenge._id, updateChallenge.scores);
            if (!executeChallenge) {
                res.send(response.failure("some error in execute challenge"));
                return;
            }
            console.log('executeChallenge', executeChallenge);
            await emitSocket('bowling_challenge_finished', executeChallenge, `${executeChallenge.title} is finished and the winner(s) ${executeChallenge.winner}`);

            /** In skill challenge winners can be multiple so pls check */
            let multipleWinners = false;
            if (executeChallenge.winner.length > 1) {
                multipleWinners = true;
            }
            console.log("multipleWinners", multipleWinners);
            if (!multipleWinners) {
                await transferAccordingToWallet(executeChallenge.winner[0].walletid, executeChallenge.winAmount, executeChallenge._id, token);
            } else {
                await asyncForEach(executeChallenge.winner, async(winner) => {
                    let amount = (executeChallenge.winAmount / executeChallenge.winner.length).toFixed(3);
                    await transferAccordingToWallet(winner.walletid, amount, executeChallenge._id, token);
                })
            }
            res.send(response.success(executeChallenge));
            return;
        }
        sendSocketForChallengeWalletDetails();
        res.send(response.success(updateChallenge));

    } catch (error) {
        console.log("error in /score", error);
        res.send(response.failure(error));
    }

});

const transferAccordingToWallet = (walletId, winAmount, challengeId, token) => {
    console.log("token ", token);
    return new Promise(async(resolve, reject) => {
        try {
            /** Check if Wallet is Gaming Wallet */
            console.log("walletId", walletId);
            console.log("winAmount", winAmount);
            let ifGamingWallet = await utils.findIfGamingWallet(walletId)
                //console.log("Gaiming wallet", ifGamingWallet);
            if (!ifGamingWallet) {
                await duckHunterCtrl.transferFundsToWinner(walletId, winAmount); // tron link wallet
                resolve(true);
            } else {
                /**Player has used his gaming wallet
                 * hence, credit funds accordingly
                 */
                console.log("gaming wallet mongoose id ", ifGamingWallet._id);
                await duckHunterCtrl.transferFundsToParentGamingWallet(winAmount);
                await duckHunterCtrl.creditFundsToGamingWallet(ifGamingWallet._id, winAmount, challengeId, token);
                resolve(true);
            }
        } catch (error) {
            console.log("error in transferAccording to wallet", error);
            reject(error);
        }

    })
}

//mongoose.Types.ObjectId(challengeId)
/**
 * return all challenges in 'open' status
 * @param size // optional
 * @param page // optional
 */
duckHunterChallenges.get('/getLiveChallenges', async(req, res) => {

    let conditionObj = {
        creator: { $exists: true, $ne: null },
        remainingPlayer: { $exists: true, $ne: 0 },
        status: 'open'
    }
    try {
        let challenges = await duckHunterModel.getAllChallenges(conditionObj, req.query.size, req.query.page);
        res.send(response.success(challenges));
    } catch (error) {
        console.log("error in getLiveChallenges", error);
        res.send(response.failure(error));
    }
})

/**
 * return all the challenges i.e open  of a player
 * @param playerId
 * * @param size // optional
 * @param page // optional
 */
duckHunterChallenges.get('/getPlayerChallenges', async(req, res) => {
    if (!req.query.playerId) {
        res.send(response.failure("Player Id (playerId) is required"));
        return;
    }
    let conditionObj = {
        creator: { $exists: true, $ne: null },
        challengers: { $elemMatch: { player: req.query.playerId } },
        $or: [{ status: 'open' }, { status: 'start' }]
    }
    try {
        let challenges = await duckHunterModel.getAllChallenges(conditionObj, req.query.size, req.query.page);
        res.send(response.success(challenges));
    } catch (error) {
        console.log("error in getPlayerChallenges", error);
        res.send(response.failure(error));
    }
})

/**
 * return all finished challenges paginated 
 * * @param size // optional
 * @param page // optional
 * is user id is provided then it will send only players finished challenges
 */
duckHunterChallenges.get('/getFinishedChallenges', async(req, res) => {

    let conditionObj = {
        creator: { $exists: true, $ne: null },
        winner: { $exists: true, $ne: null },
        challengers: {
            $elemMatch: {
                player: { $exists: true, $ne: null },
                isPlayed: true
            },
        },
        status: 'close',
    }
    if (req.query.playerId) {
        conditionObj = {
            creator: { $exists: true, $ne: null },
            winner: { $exists: true, $ne: null },
            challengers: {
                $elemMatch: {
                    player: req.query.playerId,
                    isPlayed: true
                },
            },
            status: 'close',
        }
    }
    try {
        let challenges = await duckHunterModel.getAllChallenges(conditionObj, req.query.size, req.query.page);
        res.send(response.success(challenges));
    } catch (error) {
        console.log("error in getLiveChallenges", error);
        res.send(response.failure(error));
    }
})


duckHunterChallenges.get("/deleteallskillchallenges", (req, res, next) => {
    duckHunterModel.deleteAll().then((response) => {
        console.log(response);
        res.send({ success: "success", response: response });
    }, err => {
        res.send(err);
    })
})


/********* UPDATED Code ends here *************** */
/**
 * This function comment is parsed by doctrine
 * @route POST /getskillchallenges
 * @group Skill Challenge - Contains all the APIs related to Skill Challenge
 * @param {object} NoRequest - No Request Params 
 * @returns {object} 200 - Return all skill challenges
 * @returns {Error}  default - Unexpected error
 */



module.exports = duckHunterChallenges